<!--
SPDX-FileCopyrightText: Yorhel <projects@yorhel.nl>
SPDX-License-Identifier: MIT
-->

# nginx-confgen

nginx-confgen is a preprocessor and macro system for nginx(-like)
configuration files.

# Build

Just type `make`.

# Install

```
make install PREFIX=/usr
```

# Usage

See the man page or the [website](https://dev.yorhel.nl/nginx-confgen).
